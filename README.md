mLCS
====

C++ code implimenting the mLCS method for creating streams.

A simple python wrapper (written in Cython) is now in the project so mLCS can be called
from python without having to drop write files to temporary directories for communication.


Requirements:
------------
 * [Boost](http://www.boost.org/) (for odeint)
 * [armadillo](http://arma.sourceforge.net/) (linear algebra)
 * [GSL](http://www.gnu.org/software/gsl/)
 * [Cython](http://cython.org/) (for python wrapper)

This code is tested with GCC 4.7.2 and Boost 1.57.0

# Adding New Potentials

To add a new potential use the following steps:

  1. Create the potential class in ``include/potential/{potential_name.h}``
     this should inherit from ``Potential`` or one of the helper classes for
     cylindrical or spherical potentials. Also ensure that it has a constructor
     which accepts an ``std::istringstream&``

  2. Register the class in ``include/potential/all.h`` to do this you should
     include your class here and register it to the name you want to use in
     the parameter file in ``potential_map``

  3. Add your class to ``test/TestPotentials.cc`` so that the unit tests will
     be run which will ensure that the potential and it's derivatives are
     calculated correctly.