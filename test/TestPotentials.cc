#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

#include <util/derivatives.h>

#include <potential/all.h>

#include <memory>
#include <random>
#include <typeinfo>

typedef boost::mpl::list<  Kepler
                         , NFW
                         , Hernquist
                         , MiyamotoNagai
                         , FlattenedLog
                         , TFPotential
                         , Plummer
                         , LMLogHalo
                         , TruncatedNFW
                         > static_potentials;

const double tolerance = 0.001;

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddR_along_x_axis, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    const double x = 1.0;
    const double y = 0.0;
    const double z = 0.0;
    const double t = 0.0;

    const double ddx = derivative(x, 0.001,
                                  [=](double x) {return (*pot)(x, y, z, t);} );

    BOOST_CHECK_CLOSE( ddx, pot->ddx(x,y,z,t), tolerance );
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddR_along_y_axis, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    const double x = 0.0;
    const double y = 1.0;
    const double z = 0.0;
    const double t = 0.0;

    const double ddy = derivative(y, 0.001,
                                  [=](double y) {return (*pot)(x, y, z, t);} );

    BOOST_CHECK_CLOSE( ddy, pot->ddy(x,y,z,t), tolerance );
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddz_along_z_axis, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    const double x = 0.0;
    const double y = 0.0;
    const double z = 1.0;
    const double t = 0.0;

    const double ddz = derivative(z, 0.001,
                                  [=](double z) {return (*pot)(x, y, z, t);} );

    BOOST_CHECK_CLOSE( ddz, pot->ddz(x,y,z,t), tolerance );
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddx, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double ddx = derivative(x, 0.001,
                                      [=](double x) {return (*pot)(x, y, z, t);} );

        BOOST_CHECK_CLOSE( ddx, pot->ddx(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddy, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double ddy = derivative(y, 0.001,
                                      [=](double y) {return (*pot)(x, y, z, t);} );

        BOOST_CHECK_CLOSE( ddy, pot->ddy(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_ddz, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double ddz = derivative(z, 0.001,
                                      [=](double z) {return (*pot)(x, y, z, t);} );

        BOOST_CHECK_CLOSE( ddz, pot->ddz(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dx2, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dx2 = derivative(x, 0.001,
                                        [=](double x) {return pot->ddx(x, y, z, t);} );

        BOOST_CHECK_CLOSE( d2dx2, pot->d2dx2(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dy2, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dy2 = derivative(y, 0.001,
                                        [=](double y) {return pot->ddy(x, y, z, t);} );

        BOOST_CHECK_CLOSE( d2dy2, pot->d2dy2(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dz2, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dz2 = derivative(z, 0.001,
                                        [=](double z) {return pot->ddz(x, y, z, t);} );

        BOOST_CHECK_CLOSE( d2dz2, pot->d2dz2(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dxdy, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dxdy1 = derivative(y, 0.001,
                                         [=](double y) {return pot->ddx(x, y, z, t);} );

        const double d2dxdy2 = derivative(x, 0.001,
                                         [=](double x) {return pot->ddy(x, y, z, t);} );

        // Order of differentation shouldn't matter.
        BOOST_CHECK_CLOSE( d2dxdy1, pot->d2dxdy(x,y,z,t), tolerance );
        BOOST_CHECK_CLOSE( d2dxdy2, pot->d2dxdy(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dxdz, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0); 

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dxdz1 = derivative(z, 0.001,
                                         [=](double z) {return pot->ddx(x, y, z, t);} );

        const double d2dxdz2 = derivative(x, 0.001,
                                         [=](double x) {return pot->ddz(x, y, z, t);} );

        // Order of differentation shouldn't matter.
        BOOST_CHECK_CLOSE( d2dxdz1, pot->d2dxdz(x,y,z,t), tolerance );
        BOOST_CHECK_CLOSE( d2dxdz2, pot->d2dxdz(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( test_d2dydz, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0);

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        const double d2dydz1 = derivative(z, 0.001,
                                         [=](double z) {return pot->ddy(x, y, z, t);} );

        const double d2dydz2 = derivative(y, 0.001,
                                         [=](double y) {return pot->ddz(x, y, z, t);} );

        // Order of differentation shouldn't matter.
        BOOST_CHECK_CLOSE( d2dydz1, pot->d2dydz(x,y,z,t), tolerance );
        BOOST_CHECK_CLOSE( d2dydz2, pot->d2dydz(x,y,z,t), tolerance );
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE(test_acc, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0);

    for(size_t i=0 ; i < 1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        Vec3D force;
        pot->acc(x, y, z, t, force);

        BOOST_CHECK_CLOSE( force.x, pot->ddx(x, y, z, t), 1e-16 );
        BOOST_CHECK_CLOSE( force.y, pot->ddy(x, y, z, t), 1e-16 );
        BOOST_CHECK_CLOSE( force.z, pot->ddz(x, y, z, t), 1e-16 );
    }

}

BOOST_AUTO_TEST_CASE(test_composite_acc)
{
    auto pot = std::make_shared<CompositePotential>();

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0);

    std::unique_ptr<Potential> p1(new Kepler());
    pot->potentials.push_back(std::move(p1));

    std::unique_ptr<Potential> p2(new NFW());

    for(size_t i=0 ; i<1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        Vec3D force;
        pot->acc(x, y, z, t, force);

        BOOST_CHECK_CLOSE( force.x, pot->ddx(x, y, z, t), 1e-16);
        BOOST_CHECK_CLOSE( force.y, pot->ddy(x, y, z, t), 1e-16);
        BOOST_CHECK_CLOSE( force.z, pot->ddz(x, y, z, t), 1e-16);

    }

}

BOOST_AUTO_TEST_CASE_TEMPLATE(test_rho, T, static_potentials)
{
    std::shared_ptr<Potential> pot = std::make_shared<T>();

    try {
        pot->rho(10.0, 10.0, 10.0, 0.0);
    } catch (std::exception &e) { //Skip tests for potentials without defined densities
        return;
    }

    std::mt19937 generator;
    std::uniform_real_distribution<double> coord_dist(-100., 100.0);

    for(size_t i=0 ; i<1000 ; ++i) {
        const double x = coord_dist(generator);
        const double y = coord_dist(generator);
        const double z = coord_dist(generator);
        const double t = 0.0;

        double rho = pot->d2dx2(x, y, z, t) + pot->d2dy2(x, y, z, t) + pot->d2dz2(x, y, z, t);
        rho /= (4 * pi * G);

        BOOST_CHECK_CLOSE(pot->rho(x, y, z, t), rho, tolerance);

    }
}

