#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>

#include <util/rng.h>
#include <util/vec3d.h>

BOOST_AUTO_TEST_CASE( test_random_point_in_cap )
{
    std::mt19937 generator;
    generator.seed(1234);

    std::vector<double> angles;

    const double cap_size = 0.4;

    for(size_t i = 0 ; i < 1000 ; ++i) {
        Vec3D point = point_in_cap(generator, cap_size);
        double angle = acos( point.x );
        BOOST_CHECK( angle < cap_size );
    }

}

