
BeginSat
Potential NFW 0.0035659772866 9.28311915976
mass 100.0
asat 1.0
ic 0 0 0 0 0 0
EndSat

BeginSat
Potential Plummer 0.086 1.0
mass 0.086
asat 1.0
ic 0.0 0.0 70.0 80.0 0.0 0.0
EndSat

Integration tbegin 0.0
Integration tend 4.22
Integration dt_sat 0.0001
Integration dt_phase 0.01

Stripping UniformStripping 0.0005 1 8.0 1232131
