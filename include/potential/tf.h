#pragma once

#include "static_spherical.h"
#include <consts.h>

#include <util/polyeval.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

class TFPotential : public PotentialStaticSpherical<TFPotential> {

    double v0, v02, rs, alpha, A, B, gamma, delta;

    const std::array<double, 4> A_coeff{{-0.13, 0.18, -0.36, 0.71}};
    const std::array<double, 4> B_coeff{{0.02, -0.36, 0.21, 0.22}};
    const std::array<double, 4> gamma_coeff{{-0.19, 0.0, 0.38, 0.0}};
    const std::array<double, 4> delta_coeff{{1.08, -0.69, 0.21, 1.33}};

public:
    TFPotential():v02(220.0*220.0), rs(5.0), alpha(0.6)
    {}

    TFPotential(double v0, double rs, double alpha):
        v0(v0),
        v02(v0*v0),
        rs(rs),
        alpha(alpha),
        A( polyeval(A_coeff, alpha) ),
        B( polyeval(B_coeff, alpha) ),
        gamma( polyeval(gamma_coeff, alpha) ),
        delta( polyeval(delta_coeff, alpha) )

    {}

    TFPotential(std::istringstream& iss)
    {
        iss >> v0 >> rs >> alpha;
        v02 = v0*v0;

        A = polyeval(A_coeff, alpha);
        B = polyeval(B_coeff, alpha);
        gamma = polyeval(gamma_coeff, alpha);
        delta = polyeval(delta_coeff, delta);

        if(v0 < 0) {
            throw PotentialError("v0 must be > 0 for TF potential");
        }
        if(rs < 0) {
            throw PotentialError("rs must be > 0 for TF potential");
        }
        if(alpha < 0 || alpha > 1) {
            throw PotentialError("0 < alpha < 1 for TF potential");
        }
    }

    double pot(double r) const {
        auto old_handler = gsl_set_error_handler_off();

        gsl_integration_workspace *w = gsl_integration_workspace_alloc(5000);

        double result, error;
        double params[3] = {v02, rs, alpha};

        gsl_function F;
        F.function = &pot_integrand;
        F.params = &params;

        gsl_integration_qagiu(&F, r, 1e-13, 1e-13, 5000, w, &result, &error);

        gsl_integration_workspace_free(w);

        gsl_set_error_handler(old_handler);

        return result;
    }

    double ddr(double r) const {
        return v02 * pow(1 + pow(r/rs, 2), -alpha/2) / r;
    }

    double d2dr2(double r) const {
        return -v02 * pow( 1 + pow(r/rs, 2.0), -0.5*alpha ) *
                          ((1 + alpha)*r*r + rs*rs) /
                          (r*r*(r*r + rs*rs));
    }

    double sigma(double x, double y, double z, double t) const {
        double X = sqrt(x*x + y*y + z*z) / rs;
        return v0*A / pow(B + pow(X, delta), gamma);
    }

    double rho(double x, double y, double z, double t) const {
        double X2 = (x*x + y*y + z*z) / pow(rs, 2);

        double retval = pow(1 + X2, -0.5*alpha) - alpha*X2*pow(1+X2, -1 - 0.5*alpha);
        retval *= v02 / (G * pow(rs, 2));
        return retval / (4 * pi * X2 );
    }

private:
    static double pot_integrand(double r, void *params)
    {
        double *p = static_cast<double*>(params);
        double v02 = p[0];
        double rs = p[1];
        double alpha = p[2];

        return -v02 / (r * pow(1 + pow(r/rs, 2), 0.5*alpha));
    }

};

