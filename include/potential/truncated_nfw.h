#pragma once

#include "static_spherical.h"
#include "nfw.h"

#include <consts.h>

class TruncatedNFW : public PotentialStaticSpherical<TruncatedNFW> {
    double rho0, rs, c, rvir;
    std::unique_ptr<NFW> nfw_instance;
    double m200;

public:
    TruncatedNFW() : rho0(0.001), rs(2.0), c(10.0),
                     rvir(rs*c), nfw_instance(new NFW(rho0, rs)),
                     m200(m_of_r(rvir))
    {}

    TruncatedNFW(std::istringstream &iss) {
        iss >> rho0 >> rs >> c;
        rvir = rs * c;
        nfw_instance = std::unique_ptr<NFW>(new NFW(rho0, rs));
        m200 = m_of_r(rvir);
    }

    double pot(double r) const {
        if(r > rvir) {
            return -G*m200 / r + nfw_instance->pot(rvir) + G*m200/rvir;
        }
        return nfw_instance->pot(r);
    }

    double ddr(double r) const {
        if(r > rvir) {
            return G * m200 / (r*r);
        }
        return nfw_instance->ddr(r);
    }

    double d2dr2(double r) const {
        if(r > rvir) {
            return -2*G*m200 / (r*r*r);
        }
        return nfw_instance->d2dr2(r);
    }

};