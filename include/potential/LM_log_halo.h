#pragma once

#include "base.h"
#include <consts.h>

#include <cmath>

// Anonoymous namespace for file scope
namespace {
    inline double deg2rad(double deg) {
        return pi * deg / 180.0;
    }
}

class LMLogHalo : public Potential {
    double v0, rh, q1, q2, qz, phi;
    double C1, C2, C3, v02, rh2;
public:
    LMLogHalo():v0(121.9), rh(12.0), q1(1.38), q2(1.0), qz(1.36), phi(deg2rad(97.0))
    {
        init_consts();
    }

    LMLogHalo(double v0, double rh, double q1, double q2, double qz, double phi):
        v0(v0), rh(rh), q1(q1), q2(q2), qz(qz), phi(phi)
    {
        init_consts();
    }

    LMLogHalo(std::istringstream& iss)
    {
        iss >> v0 >> rh >> q1 >> q2 >> qz >> phi;
        init_consts();
    }

    double operator()(double x, double y, double z, double t = 0) const {
        return v02 * log(C1*x*x + C2*y*y + C3*x*y + pow(z/qz, 2) + rh2);
    }

    double ddx(double x, double y, double z, double t = 0) const {
        double log_arg = C1*x*x + C2*y*y + C3*x*y + pow(z/qz, 2) + rh2;
        return v02 * (2*C1*x + C3*y) / log_arg;
    }

    double ddy(double x, double y, double z, double t = 0) const {
        double log_arg = C1*x*x + C2*y*y + C3*x*y + pow(z/qz, 2) + rh2;
        return v02 * (C3*x + 2*C2*y) / log_arg;
    }

    double ddz(double x, double y, double z, double t = 0) const {
        double log_arg = C1*x*x + C2*y*y + C3*x*y + pow(z/qz, 2) + rh2;
        return 2 * v02 * z / (pow(qz, 2) * log_arg);
    }

    double d2dr2(double x, double y, double z, double t = 0) const {
        return 0.0;
    }

private:
    void init_consts()
    {
        C1 = pow(cos(phi) / q1, 2) + pow(sin(phi) / q2, 2);
        C2 = pow(cos(phi) / q2, 2) + pow(sin(phi) / q1, 2);
        C3 = 2 * sin(phi) * cos(phi) * (pow(q1, -2) - pow(q2, -2));
        v02 = pow(v0, 2);
        rh2 = pow(rh, 2);
    }
};