#pragma once

#include <memory>
#include <iterator>


// Evaluate a polynomial using Horner's method.
// Coefficients are assumed to be stored with the highest power first.
template <class Container, typename T>
T polyeval(const Container & coeffs, const T x)
{
    auto it = std::begin(coeffs);
    T b = *(it++);

    for(auto end = std::end(coeffs) ; it != end ; ++it) {
        b = *it + b*x;
    }

    return b;
}

