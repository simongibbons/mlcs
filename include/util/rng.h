#pragma once

#include <random>
#include <util/vec3d.h>

// Generate a point in a cap centered on the x axis in an aperture
// of width opening_angle
Vec3D point_in_cap(std::mt19937 &generator, double opening_angle);

