#pragma once

#include <vector>
#include <memory>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

class spline {
private:
  const std::vector<double> m_x, m_y;
  std::unique_ptr<gsl_interp_accel, void(*)(gsl_interp_accel*)> acc;
  std::unique_ptr<gsl_spline, void(*)(gsl_spline*)> spl;
public:

  spline(const std::vector<double>& x,
         const std::vector<double>& y):
    m_x(x),
    m_y(y),
    acc( gsl_interp_accel_alloc(), gsl_interp_accel_free ),
    spl( gsl_spline_alloc(gsl_interp_linear, m_x.size()),
         gsl_spline_free )
  {
    gsl_spline_init(spl.get(), m_x.data(), m_y.data(), m_x.size());
  }

  spline(const spline& other):
    m_x(other.get_x()),
    m_y(other.get_y()),
    acc( gsl_interp_accel_alloc(), gsl_interp_accel_free),
    spl( gsl_spline_alloc(gsl_interp_linear, m_x.size()),
         gsl_spline_free)
  {
    gsl_spline_init(spl.get(), m_x.data(), m_y.data(), m_x.size());
  }

  double operator() (double x) const
  {
    return gsl_spline_eval(spl.get(), x, acc.get());
  }

  std::vector<double> get_x() const
  {
    return m_x;
  }

  std::vector<double> get_y() const
  {
    return m_y;
  }

  size_t data_index(double x) const
  {
    return gsl_interp_accel_find(acc.get(), m_x.data(), m_x.size(), x);
  }

  void set_acc_index(size_t idx) const
  {
    acc->cache = idx;
  }

};

