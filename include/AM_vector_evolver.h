#pragma once

#include <vector>
#include <array>
#include <memory>
#include <functional>

#include <util/spline.h>
#include <util/vec3d.h>

#include <satellites.h>

#include <armadillo>
#include <boost/numeric/odeint.hpp>


namespace {

using namespace boost::numeric::odeint;

typedef std::array<double, 3> amStateType;
typedef runge_kutta_cash_karp54< amStateType > amErrorStepperType;

struct AMVectorIntegratorFunctor {


    const Satellites &sats;
    const std::shared_ptr<Potential> pot;

    const double I1, I2, I3;

    AMVectorIntegratorFunctor( const Satellites &sats,
                               const std::shared_ptr<Potential> pot,
                               double I1, double I2, double I3 ):sats(sats),
                                                                 pot(pot),
                                                                 I1(I1),
                                                                 I2(I2),
                                                                 I3(I3)
    {}

    void operator()(const amStateType &s, amStateType &dsdt, double t)
    {
        using namespace arma;

        Vec3D satpos( sats.x(0,t), sats.y(0,t), sats.z(0,t) );

        Vec3D rhat = satpos / satpos.length();

        // e3 is parallel to the AM of the progenitor
        // e1 and e2 are in the plane of the progenitor
        Vec3D e3 = Vec3D(s[0], s[1], s[2]);
        e3.normalise();
        Vec3D e1 = rhat - (e3*rhat)*e3;
        e1.normalise();
        Vec3D e2 = e3^e1;

        // Transformation between global frame and satellite frame
        mat33 glob_to_sat;
        glob_to_sat << e1.x << e1.y << e1.z << endr
                    << e2.x << e2.y << e2.z << endr
                    << e3.x << e3.y << e3.z << endr;

        mat33 sat_to_glob = glob_to_sat.i();

        mat33 hess_glob = pot->hessian( satpos.x, satpos.y, satpos.z, t );
        mat33 hess_sat  = sat_to_glob.t() * hess_glob * sat_to_glob;


        // Components of torques on progenitor in the disk frame
        vec3 tau_sat;
        tau_sat << hess_sat(1,2)*(I2 - I3)
                << hess_sat(0,2)*(I1 - I3)
                << 0.0;

        vec3 tau_glob = sat_to_glob * tau_sat;

        dsdt[0] = tau_glob(0);
        dsdt[1] = tau_glob(1);
        dsdt[2] = tau_glob(2);

    }

};

}

class AMVectorIntegratorObserver {

public:
    std::array< std::vector<double>, 3 > &amComponents;
    std::vector< double > &times;

    AMVectorIntegratorObserver( std::array< std::vector<double>, 3 > &amComponents,
                                std::vector< double > &times ):amComponents(amComponents),
                                                               times(times)
    {}

    ~AMVectorIntegratorObserver()
    {}

    void operator()(const amStateType &s, double t)
    {
        times.push_back(t);
        for( size_t i = 0 ; i < 3 ; ++i ) {
            amComponents[i].push_back( s[i] );
        }
    }

};



// Takes an initial angular momentum vector and evolves it
// as a disk should be torqued.
class AMVector {
    spline x, y, z;
public:

    AMVector( const spline &x, const spline &y, const spline &z )
        : x(x), y(y), z(z)
    {}

    Vec3D operator ()(double t) const
    {
        return Vec3D(x(t), y(t), z(t));
    }

};

AMVector make_AMVector(const Vec3D &initial,
                       const std::shared_ptr<Potential> pot,
                       double rd, double zd, const Satellites &sats, double dt)
{
    const double epsAbs = 1e-4;
    const double epsRel = 1e-4;

    double I1 = 3 * pow(rd, 2);
    double I2 = I1;
    double I3 = (1./3.) * pow(pi,2) * pow(zd, 2);


    AMVectorIntegratorFunctor odeRhs( sats, pot, I1, I2, I3 );

    amStateType ic({{initial.x, initial.y, initial.z}});

    std::vector<double> times;
    std::array< std::vector<double>, 3 > stateVecs;
    AMVectorIntegratorObserver odeObs( stateVecs, times );

    auto stepper = make_controlled( epsAbs, epsRel, amErrorStepperType() );
    integrate_const(stepper, odeRhs, ic, sats.tstart, sats.tend, dt, odeObs );

    spline x(times, stateVecs[0]);
    spline y(times, stateVecs[1]);
    spline z(times, stateVecs[2]);

    return AMVector( std::move(x), std::move(y), std::move(z) );
}

std::function<Vec3D(double)> make_AM_functor(const AMVector am)
{
    return [am](double t) { return am(t);};
}

std::function<Vec3D(double)> make_const_AM_functor(const Vec3D am) {
    return [am](double t) { return am; };
}

