#pragma once

#include <satellite.h>
#include <consts.h>
#include <potential/base.h>
#include <util/spline.h>

#include <memory>
#include <vector>
#include <array>
#include <algorithm>

#include <boost/numeric/odeint.hpp>

namespace{

using namespace boost::numeric::odeint;

typedef std::vector<double> satsStateType;
typedef runge_kutta_cash_karp54< satsStateType > satsErrorStepperType;

struct OrbitIntegrateFunctor {

    std::vector<Satellite> sats;
    const size_t nsats;

    OrbitIntegrateFunctor( const std::vector<Satellite> &sats ):
        sats(sats), nsats(sats.size())
    {}

    void operator()( const satsStateType &s, satsStateType &dsdt, double t )
    {
        Vec3D loc;
        Vec3D satloc;
        Vec3D satvel;
        Vec3D vel;
        Vec3D force;
        Vec3D force_df;

        for(size_t i=0 ; i<nsats ; ++i ) {
            loc.x = s[6*i + 0];
            loc.y = s[6*i + 1];
            loc.z = s[6*i + 2];

            dsdt[6*i + 0] = s[6*i + 3];
            dsdt[6*i + 1] = s[6*i + 4];
            dsdt[6*i + 2] = s[6*i + 5];

            dsdt[6*i + 3] = 0;
            dsdt[6*i + 4] = 0;
            dsdt[6*i + 5] = 0;

            for( size_t j=0 ; j<nsats ; ++j ) {
                //no self force
                if(j == i) continue;

                satloc.x = s[6*j + 0];
                satloc.y = s[6*j + 1];
                satloc.z = s[6*j + 2];

                sats[j].acc(satloc, loc, t, force);

                //Dynamical Friction from Body 0 (the MW) acts on the non
                //stream generating sats.
                if(j == 0 && i >= 1) {
                    satvel.x = s[6*j + 3];
                    satvel.y = s[6*j + 4];
                    satvel.z = s[6*j + 5];

                    vel.x = s[6*i + 3];
                    vel.y = s[6*i + 4];
                    vel.z = s[6*i + 5];

                    sats[j].dynamical_friction(satloc, loc, satvel, vel, t, sats[i].mass, force_df);
                    force -= force_df;
                }

                dsdt[6*i + 3] -= force.x;
                dsdt[6*i + 4] -= force.y;
                dsdt[6*i + 5] -= force.z;
            }
        }

        for(size_t i = 0 ; i < 6 ; ++i)
            dsdt[i] = 0.;
    }

};

class OrbitIntegratorObserver {

public:
    std::vector< std::vector< double > >& stateVecs;
    std::vector< double >& times;

    OrbitIntegratorObserver(std::vector< std::vector< double > > &stateVecs,
                            std::vector< double > &times):stateVecs(stateVecs),
                                                          times(times)
    {}

    ~OrbitIntegratorObserver()
    {}

    void operator()(const satsStateType &s, double t)
    {
        times.push_back(t);

        for( size_t i = 0 ; i < stateVecs.size() ; ++i ) {
            stateVecs[i].push_back( s[i] );
        }
    }

};

class Satellites : public Potential
{
    std::vector<Satellite> sats;

    double epsAbs = 1e-10;
    double epsRel = 1e-10;

    std::vector< std::array< spline, 6 > > satorbits;

public:
    size_t nsats;
    double tstart;
    double tend;

    Satellites()
    {}

    Satellites(const std::vector<Satellite> &sats,
               const std::vector< std::array<double, 6>> ics,
               double t0,
               double t1,
               double dt):
        sats(sats),
        satorbits(),
        nsats(sats.size()), 
        tstart(std::min(t0,t1)),
        tend(std::max(t0,t1))
    {
        std::vector<double> times;
        std::vector< std::vector<double> > stateVecs( nsats*6 );
        OrbitIntegratorObserver odeObs(stateVecs, times);
        OrbitIntegrateFunctor odeRhs(sats);

        std::vector<double> icInternal;

        dt = t0 < t1 ? fabs(dt) : -fabs(dt);

        for(auto s = ics.begin() ; s != ics.end() ; ++s) {
            for(size_t i=0 ; i<6 ; ++i) {
                icInternal.push_back( (*s)[i] );
            }
        }

        //Have one euler step backwards to deal with interpolation errors
        times.push_back(t0 - dt);
        for(size_t i=0 ; i<ics.size() ; ++i ) {
            stateVecs[6*i + 0].push_back( ics[i][0] - dt*ics[i][3] );
            stateVecs[6*i + 1].push_back( ics[i][1] - dt*ics[i][4] );
            stateVecs[6*i + 2].push_back( ics[i][2] - dt*ics[i][5] );
            stateVecs[6*i + 3].push_back( ics[i][3] );
            stateVecs[6*i + 3].push_back( ics[i][4] );
            stateVecs[6*i + 3].push_back( ics[i][5] );
        }

        auto stepper = make_controlled(epsAbs, epsRel, satsErrorStepperType() );
        if(dt < 0.0 ) {
            integrate_const(stepper, odeRhs, icInternal, t0, t1 - 0.1, dt, odeObs);
        } else {
            integrate_const(stepper, odeRhs, icInternal, t0, t1 + 0.1, dt, odeObs);
        }

        //Reverse the output if necessary
        if( t1 < t0 ) {
            std::reverse( times.begin(), times.end() );
            std::for_each( stateVecs.begin(), stateVecs.end(),
                           [](std::vector<double> &v) {
                               std::reverse(v.begin(), v.end());
                           });
        }

        for( size_t i = 0 ; i < nsats ; ++i ) {
            satorbits.emplace_back( std::array<spline,6> {{
                spline{times, stateVecs[6*i + 0]},
                spline{times, stateVecs[6*i + 1]},
                spline{times, stateVecs[6*i + 2]},
                spline{times, stateVecs[6*i + 3]},
                spline{times, stateVecs[6*i + 4]},
                spline{times, stateVecs[6*i + 5]}
            }});
        }
    }

    double operator()(double x, double y, double z, double t) const
    {
        return 0.0;
    }

    double d2dr2(double x, double y, double z, double t) const
    {
        return 0.0;
    }

    void acc(double x, double y, double z, double t, Vec3D& force) const
    {
        Vec3D satloc;
        Vec3D pos(x, y, z);
        Vec3D new_force;

        force.zero();

        for(size_t i=0 ; i<nsats ; ++i) {
            satloc.x = this->x(i, t);
            satloc.y = this->y(i, t);
            satloc.z = this->z(i, t);

            sats[i].acc(satloc, pos, t, new_force);
            force += new_force;
        }
    }

    double x(size_t sat_id, double t) const {
        return satorbits[sat_id][0](t);
    }

    double y(size_t sat_id, double t) const {
        return satorbits[sat_id][1](t);
    }

    double z(size_t sat_id, double t) const {
        return satorbits[sat_id][2](t);
    }

    double vx(size_t sat_id, double t) const {
        return satorbits[sat_id][3](t);
    }

    double vy(size_t sat_id, double t) const {
        return satorbits[sat_id][4](t);
    }

    double vz(size_t sat_id, double t) const {
        return satorbits[sat_id][5](t);
    }

    Vec3D get_pos(size_t sat_id, double t) const {
        return Vec3D( x(sat_id, t), y(sat_id, t), z(sat_id, t) );
    }

    Vec3D get_vel(size_t sat_id, double t) const {
        return Vec3D( vx(sat_id, t), vy(sat_id, t), vz(sat_id, t) );
    }

    //Tidal radius of a satellite - always assumes the MW
    //is the zeroth satellite member.
    double rtide(size_t sat_id, double t) const {
        // Satellite position in the MW frame
        const auto sat_pos = get_pos(sat_id, t) - get_pos(0, t);
        const auto sat_vel = get_vel(sat_id, t) - get_vel(0, t);

        const double r = sat_pos.length();
        const double vr = (sat_pos * sat_vel) / r;
        const double omega2 = (sat_vel.lengthsq() - vr*vr) / (r*r);

        return pow(G*sats[sat_id].mass / (omega2 - sats[sat_id].pot->d2dr2(sat_pos.x, sat_pos.y, sat_pos.z, t)), 1.0/3.0);
    }

    double get_asat(size_t sat_id) const {
        return sats[sat_id].asat;
    }

    double get_mass(size_t sat_id) const {
        return sats[sat_id].mass;
    }

    double r(size_t sat_id, double t) const {
        Vec3D mwpos = get_pos(0, t);
        Vec3D satpos = get_pos(sat_id, t);

        return (satpos - mwpos).length();
    }

};

} //namespace
