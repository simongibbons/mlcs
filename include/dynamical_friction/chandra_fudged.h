#pragma once

#include "base.h"

#include <potential/base.h>
#include <memory>
#include <cmath>
#include <gsl/gsl_sf_erf.h>
#include <consts.h>


class ChandrasakarFudged : public DynamicalFriction {

    double eps;

public:
    ChandrasakarFudged(std::istringstream &iss)
    {
        iss >> eps;
    }

    ChandrasakarFudged(double eps) : eps(eps)
    {}

    void acc(double x, double y, double z, double vx,
             double vy, double vz, double t,
             const std::shared_ptr<Potential> & pot,
             double m, Vec3D &force) const
    {
        double r = sqrt(x*x + y*y + z*z);

        if(r < eps) { //Turn off DF if r < eps (therefore an unphysical acceleration
            force.x = 0;
            force.y = 0;
            force.z = 0;

            return;
        }

        double sigma = pot->sigma(x, y, z, t);
        double v = sqrt(vx*vx + vy*vy + vz*vz);
        double X = v / (sqrt(2.0) * sigma);
        double rho = pot->rho(x, y, z, t);
        double coulomb_log = log(r/eps);

        double prefactor = -4*pi*pow(G,2)*m*rho*coulomb_log / pow(v, 3);
        prefactor *= (gsl_sf_erf(X) - 2*X * exp(-X*X) / sqrt(pi));

        force.x = vx * prefactor;
        force.y = vy * prefactor;
        force.z = vz * prefactor;
    }

};

