#pragma once

#include <util/vec3d.h>

class DynamicalFriction {

public:
    virtual void acc(double x, double y, double z,
                     double vx, double vy, double vz,
                     double t, const std::shared_ptr<Potential>& pot,
                     double m, Vec3D &force) const = 0;

    virtual ~DynamicalFriction()
    {}

};

