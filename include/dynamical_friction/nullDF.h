#pragma once

#include "base.h"

#include <potential/base.h>

#include <sstream>
#include <memory>

class NullDF : public DynamicalFriction {
public:
    NullDF()
    {}

    NullDF(std::istringstream& iss)
    {}

    void acc(double x, double y, double z,
             double vx, double vy, double vz,
             double t, const std::shared_ptr<Potential> &pot,
             double m, Vec3D &force) const
    {
        force.x = 0;
        force.y = 0;
        force.z = 0;
    }
};

