#pragma once

#include <unordered_map>
#include <memory>
#include <functional>

#include "base.h"

#include "nullDF.h"
#include "chandra_fudged.h"


using df_function_t = std::function<std::unique_ptr<DynamicalFriction>(std::istringstream&)>;

template <typename T>
std::unique_ptr<DynamicalFriction> df_maker(std::istringstream& iss)
{
    return std::unique_ptr<DynamicalFriction>(new T(iss));
}

const std::unordered_map<std::string, df_function_t> df_map = {
    {"NoDF", df_maker<NullDF>},
    {"ChandrasakarFudged", df_maker<ChandrasakarFudged>}
};

