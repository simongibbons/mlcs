#pragma once

#include <memory>
#include <potential/base.h>
#include <dynamical_friction/base.h>
#include <dynamical_friction/nullDF.h>

#include <util/vec3d.h>

class Satellite {
public:
    double mass; // Mass used when stripping.
    double asat; // Scale radius used when stripping.
    std::shared_ptr<Potential> pot;
    std::shared_ptr<DynamicalFriction> df;

    Satellite(double mass, double asat,
              std::shared_ptr<Potential> pot,
              std::shared_ptr<DynamicalFriction> df = std::make_shared<NullDF>()):
        mass(mass), asat(asat), pot(pot), df(df)
    {}

    // Evaluate the force at a point sourced by the satellite
    void acc(const Vec3D& satloc, const Vec3D &pos, double t, Vec3D& force) const
    {
        double dx = pos.x - satloc.x;
        double dy = pos.y - satloc.y;
        double dz = pos.z - satloc.z;

        pot->acc(dx, dy, dz, t, force);
    }

    // Evaluate the Dynamical friction caused by the satellite.
    void dynamical_friction(const Vec3D& satloc, const Vec3D &pos,
                            const Vec3D& satvel, const Vec3D &vel,
                            double t, double m, Vec3D& force) const
    {
        double dx = pos.x - satloc.x;
        double dy = pos.y - satloc.y;
        double dz = pos.z - satloc.z;

        double dvx = vel.x - satvel.x;
        double dvy = vel.y - satvel.y;
        double dvz = vel.z - satvel.z;

        df->acc(dx, dy, dz, dvx, dvy, dvz, t, pot, m, force);
    }
};

