#pragma once

#include <potential/base.h>
#include <satellites.h>

#include <util/vec3d.h>

#include <array>
#include <vector>
#include <random>
#include <exception>

#include <ic_gens/base.h>

#include <boost/numeric/odeint.hpp>

namespace {

class StopIntegration : public std::exception
{};

using namespace boost::numeric::odeint;

typedef std::array<double, 6> debrisStateType;
typedef runge_kutta_cash_karp54< debrisStateType > debrisErrorStepperType;

struct DebrisIntegratorFunctor {

    const Satellites& sats;
    Vec3D force;

    DebrisIntegratorFunctor( const Satellites &sats ):sats(sats)
    {}

    void operator()(const debrisStateType &s, debrisStateType &dsdt, double t)
    {
        dsdt[0] = s[3];
        dsdt[1] = s[4];
        dsdt[2] = s[5];

        sats.acc(s[0], s[1], s[2], t, force);

        dsdt[3] = -force.x;
        dsdt[4] = -force.y;
        dsdt[5] = -force.z;
    }

};

struct DebrisObserverFunctor {

    const Satellites &sats;
    const double tcreate;
    double &tstrip;
    double &phase;
    double &tloc;

    DebrisObserverFunctor( const Satellites &sats,
                           double tcreate,
                           double &tstrip,
                           double &phase,
                           double &tloc ):sats(sats),
                                          tcreate(tcreate),
                                          tstrip(tstrip),
                                          phase(phase),
                                          tloc(tloc)
    {}

    void operator()(const debrisStateType &s, double t)
    {
        const Vec3D mwpos = sats.get_pos(0, t);
        const Vec3D satpos = sats.get_pos(1, t);
        const Vec3D debrispos(s[0], s[1], s[2]);

        const double r = sqrt( pow(s[0] - mwpos.x,2) +
                               pow(s[1] - mwpos.y,2) +
                               pow(s[2] - mwpos.z,2) );
        const double rtide = sats.rtide(1,t);
        const double satdist = (satpos - mwpos).length();

        phase += r - satdist;
        // if( (satdist < rtide*1.2) && ((t - tstrip) < 1.0) ) {
        //     tstrip = t;
        // }

        const double dist = (debrispos - satpos).length();

        if((dist < 1.2*rtide) && (t - tstrip) < 1.0) {
            tstrip = t;
        }

        //if((tstrip - tcreate) > 0.4) {
            //throw StopIntegration();
        //}

        tloc = t;
    }

};

class Debris {

    const double epsRel = 1e-8;
    const double epsAbs = 1e-8;

public:

    std::vector<double> xs,ys,zs,vxs,vys,vzs;
    std::vector<double> tstrips;
    std::vector<double> tcreate;
    std::vector<double> phases;

public:
    Debris(const Satellites& sats,
           std::shared_ptr<DebrisICGen> icgenerator,
           double dtphase)
    {
        // Make the ics of the debris

        Particles parts = icgenerator->make_debrisIC( sats );
        std::vector< std::array<double, 6> > ics( parts.state );

        //Evolve the debris to tfinal.
        const size_t nparts = ics.size();

        xs.resize(nparts);
        ys.resize(nparts);
        zs.resize(nparts);
        vxs.resize(nparts);
        vys.resize(nparts);
        vzs.resize(nparts);
        tstrips.resize(nparts);
        tcreate.resize(nparts);
        phases.resize(nparts);

        DebrisIntegratorFunctor odeRhs(sats);

        #pragma omp parallel for schedule(dynamic, 4)
        for( size_t i = 0 ; i < nparts ; ++i ) {
            auto stepper = make_controlled( epsAbs, epsRel, debrisErrorStepperType() );
            double tloc = parts.t[i];
            double tstrip = parts.t[i];
            double phase = 0.0;

            DebrisObserverFunctor obs( sats, parts.t[i], tstrip, phase, tloc );
            try{
                integrate_const( stepper, odeRhs, ics[i], tstrip, sats.tend, dtphase, obs);
                if( tloc < sats.tend ) {
                    integrate_const( stepper, odeRhs, ics[i], tloc, sats.tend, sats.tend - tloc, obs);
                }
            } catch(const StopIntegration& e) {
                continue;
            }
            xs[i] = ics[i][0];
            ys[i] = ics[i][1];
            zs[i] = ics[i][2];
            vxs[i] = ics[i][3];
            vys[i] = ics[i][4];
            vzs[i] = ics[i][5];
            tcreate[i] = parts.t[i];
            tstrips[i] = tstrip;
            phases[i] = phase;
        }

    }

};

}
