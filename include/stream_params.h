#pragma once

#include <memory>
#include <vector>
#include <fstream>
#include <sstream>

#include <potential/base.h>
#include <ic_gens/base.h>
#include "satellite.h"

struct StreamParams {

    std::vector<Satellite> sats;
    std::shared_ptr<DebrisICGen> strip_method;
    std::vector< std::array<double, 6> > ics;

    double tbegin;
    double tend;
    double dt_sat = 0.0001;
    double dt_phase = 0.01;

};

StreamParams read_params(const std::string& s_in );

