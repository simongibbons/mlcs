#pragma once

#include "base.h"

#include <util/vec3d.h>
#include <util/rng.h>

#include <functional>
#include <iostream>
#include <fstream>
#include <random>
#include <exception>


class BurstyStripping : public DebrisICGen {

    double dtcreate;
    size_t numPerCreation;
    std::mt19937 generator;
    std::normal_distribution<double> distribution;

    double burstWidth;

    std::function<double(double)> strip_prob_fn = [](double t) {return 1.0;};
    bool created_strip_fn = false;

    int seed;

public:
    BurstyStripping(double dtcreate, size_t numPerCreation, double sigma, double burstWidth, double openingAngle, int seed):
        dtcreate(dtcreate),
        numPerCreation(numPerCreation),
        generator(),
        distribution(0.0, sigma),
        burstWidth(burstWidth)
    {
        generator.seed(seed);
    }

    BurstyStripping(std::istringstream& iss)
    {
        double sigma;
        int seed;

        if( !(iss >> dtcreate >> numPerCreation >> sigma >> burstWidth >> seed) )
        {
            throw std::runtime_error("PARSE ERROR: BurstyStripping expects the parameters: "
                                     "dtcreate numPerCreation sigma burstWidth seed");
        }

        distribution = std::normal_distribution<double>(0.0, sigma);
        generator.seed(seed);
    }

    void output_strip_prob_fn( std::string fname, const Satellites &sats ) {
        strip_prob_fn = make_strip_probability_func(sats);
        created_strip_fn = true;

        std::ofstream fout;
        fout.open( fname, std::ofstream::out );
        if( fout.fail() ) {
            std::cerr << "ERROR: Cannot open file " << fname << " for writing\n";
            return;
        }

        for( double t = sats.tstart; t < sats.tend ; t += 0.001 ) {
            fout << t << " " << strip_prob_fn(t) << std::endl;
        }
        fout.close();
    }

    Particles make_debrisIC(const Satellites &sats) {
        Particles debris;

        if( !created_strip_fn ) {
            strip_prob_fn = make_strip_probability_func(sats);
            created_strip_fn = true;
        }

        double strip_prob_max = 0.0;
        for( double t = sats.tstart ; t < sats.tend ; t += 0.0001 ) {
            double sp = strip_prob_fn(t);
            strip_prob_max = sp > strip_prob_max ? sp : strip_prob_max;
        }

        std::uniform_real_distribution<double> strip_prob_dist(0.0, strip_prob_max);

        for( double t = sats.tstart ; t < sats.tend ; t += dtcreate ) {
            if( strip_prob_dist(generator) > strip_prob_fn(t) ) {
                continue;
            }

            double rtide = sats.rtide(1, t);

            Vec3D mwpos = sats.get_pos(0, t);
            Vec3D mwvel = sats.get_vel(0, t);

            Vec3D satpos = sats.get_pos(1, t) - mwpos;
            Vec3D satvel = sats.get_vel(1, t) - mwvel;

            double rsat = satpos.length();
            double vrsat = satpos*satvel / rsat;
            double vtsat = sqrt( satvel.lengthsq() - pow(vrsat, 2) );

            Vec3D rhat(satpos);
            rhat.normalise();
            Vec3D that = satvel - rhat*vrsat;
            that.normalise();

            Vec3D oophat = (rhat^that);

            double vr, vt, voop;
            std::array<double, 6> ic;

            for( size_t i=0 ; i < numPerCreation ; ++i ) {

                //Trailing
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                ic[0] = satpos.x + rhat.x*rtide + mwpos.x;
                ic[1] = satpos.y + rhat.y*rtide + mwpos.y;
                ic[2] = satpos.z + rhat.z*rtide + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back(ic);
                debris.t.push_back(t);
                debris.phases.push_back(0.0);

                //Leading
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                ic[0] = satpos.x - rhat.x*rtide + mwpos.x;
                ic[1] = satpos.y - rhat.y*rtide + mwpos.y;
                ic[2] = satpos.z - rhat.z*rtide + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back( ic );
                debris.t.push_back(t);
                debris.phases.push_back(0.0);


            }

        }

        return debris;
    }

private:
    std::function<double (double)> make_strip_probability_func(const Satellites &sats)
    {
        std::vector<double> peri_times(find_peri_times(sats));
        std::vector<double> widths;

        //Add an initial period of stripping as well before we have orbit
        //integration data. Assume that the period is the same as the last one
        //(should possibly change this in the future........)
        double radial_period;

        try {
            radial_period = peri_times.at(1) - peri_times.at(0);
        } catch (const std::out_of_range &e) {
            std::cerr << "Need to see two peri crossings to determine period - \n"
                         "Bug Simon to fix this.....\n";
            exit(1);
        }

        peri_times.insert(peri_times.begin(), peri_times.front() - radial_period);
        peri_times.insert(peri_times.begin(), peri_times.front() - radial_period);

        peri_times.push_back(peri_times.back() + radial_period);
        peri_times.push_back(peri_times.back() + radial_period);

        for(size_t i = 0 ; i < peri_times.size() - 1 ; ++i ) {
            widths.push_back(burstWidth * (peri_times[i+1] - peri_times[i]));
        }
        widths.push_back(widths.back());

        return [peri_times, widths](double t) {
            double retval = 0.0;
            for(size_t i = 0 ; i < peri_times.size() ; ++i ) {
                retval += exp( -0.5*pow((t - peri_times[i]) / widths[i], 2));
            }
            return retval;
        };
    }

    std::vector<double> find_peri_times(const Satellites &sats)
    {
        std::vector<double> peri_times;
        const double dt = 0.0001;

        for( double t = sats.tstart ; t < sats.tend ;  t += dt ) {
            double r = sats.r(1,t);
            if( sats.r(1, t - dt) > r && sats.r(1, t + dt) > r ) {
                peri_times.push_back(t);
            }
        }

        return peri_times;
    }


};

