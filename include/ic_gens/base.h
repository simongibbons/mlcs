#pragma once

#include <vector>
#include <array>

#include <satellites.h>

struct Particles {
    std::vector< double > t;
    std::vector< double > phases;
    std::vector< std::array<double, 6> > state;
};

class DebrisICGen {
public:
    virtual Particles make_debrisIC(const Satellites &sats) = 0;

    virtual void reset_rng()
    {}

    virtual void output_strip_prob_fn( std::string fname, const Satellites &sats )
    {
        //std::cerr << "ERROR: No Strip Prob fn defined for this method\n";
    }

    virtual ~DebrisICGen()
    {}
};

