#pragma once

#include <unordered_map>
#include <memory>
#include <functional>

#include "base.h"

#include "bursty_rtide.h"
#include "no_debris.h"
#include "uniform_stripping.h"
#include "rotating_bursty_fixed.h"
#include "bursty_stripping.h"

using ic_function_t = std::function<std::shared_ptr<DebrisICGen>(std::istringstream&)>;

template <typename T>
std::shared_ptr<DebrisICGen> strip_method_maker( std::istringstream& iss )
{
    return std::make_shared<T>(iss);
}

const std::unordered_map<std::string, ic_function_t> ic_map = {
    {"UniformStripping",  strip_method_maker<UniformStripping>},
    {"BurstyRtide", strip_method_maker<BurstyRtide>},
    {"RotatingBurstyFixed", strip_method_maker<RotatingBurstyFixed>},
    {"BurstyStripping", strip_method_maker<BurstyStripping>},
    {"NoDebris", strip_method_maker<NoDebris>}
};

