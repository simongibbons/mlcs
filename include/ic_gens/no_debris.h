#pragma once

#include "base.h"

#include <sstream>

class NoDebris : public DebrisICGen {

public:
    NoDebris(std::istringstream& iss)
    {}

    Particles make_debrisIC(const Satellites &sats)
    {
        return Particles();
    }

};

