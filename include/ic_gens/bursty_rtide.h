#pragma once

#include "base.h"

#include <util/vec3d.h>
#include <util/rng.h>

#include <functional>
#include <iostream>
#include <fstream>
#include <random>
#include <exception>


class BurstyRtide : public DebrisICGen {

    double dtcreate;
    size_t numPerCreation;
    std::mt19937 generator;
    std::normal_distribution<double> distribution;

    double openingAngle;

    std::function<double(double)> strip_prob_fn = [](double t) {return 1.0;};
    bool created_strip_fn = false;

    int seed;

public:
    BurstyRtide(std::istringstream& iss)
    {
        double sigma;
        int seed;

        openingAngle = 0.0;

        if( !(iss >> dtcreate >> numPerCreation >> sigma >> seed) )
        {
            throw std::runtime_error("PARSE ERROR: BurstyStripping expects the parameters: "
                                     "dtcreate numPerCreation sigma seed");
        }

        distribution = std::normal_distribution<double>(0.0, sigma);
        generator.seed(seed);
    }

    void output_strip_prob_fn( std::string fname, const Satellites &sats ) {
        strip_prob_fn = make_strip_probability_func(sats);
        created_strip_fn = true;

        std::ofstream fout;
        fout.open( fname, std::ofstream::out );
        if( fout.fail() ) {
            std::cerr << "ERROR: Cannot open file " << fname << " for writing\n";
            return;
        }

        for( double t = sats.tstart; t < sats.tend ; t += 0.001 ) {
            fout << t << " " << strip_prob_fn(t) << std::endl;
        }
        fout.close();
    }

    Particles make_debrisIC(const Satellites &sats) {
        Particles debris;

        std::uniform_real_distribution<double> theta_dist(pi/2 - openingAngle, pi/2 + openingAngle);
        std::uniform_real_distribution<double> phi_dist(0.0, 2*pi);

        if( !created_strip_fn ) {
            strip_prob_fn = make_strip_probability_func(sats);
            created_strip_fn = true;
        }

        double strip_prob_max = 1.0;

        std::uniform_real_distribution<double> strip_prob_dist(0.0, strip_prob_max);

        for( double t = sats.tstart ; t < sats.tend ; t += dtcreate ) {
            double rtide = sats.rtide(1, t);

            if( strip_prob_dist(generator) > strip_prob_fn(t) ) {
                continue;
            }

            Vec3D mwpos = sats.get_pos(0, t);
            Vec3D mwvel = sats.get_pos(0, t);

            Vec3D satpos = sats.get_pos(1, t) - mwpos;
            Vec3D satvel = sats.get_vel(1, t) - mwvel;

            double rsat = satpos.length();
            double vrsat = satpos*satvel / rsat;
            double vtsat = sqrt( satvel.lengthsq() - pow(vrsat, 2) );

            Vec3D rhat(satpos);
            rhat.normalise();
            Vec3D that = satvel - rhat*vrsat;
            that.normalise();
            Vec3D oophat = (rhat^that);

            double vr, vt, voop, dR, dT, doop;
            Vec3D launch_point;
            std::array<double, 6> ic;

            for( size_t i=0 ; i < numPerCreation ; ++i ) {

                //Trailing
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                launch_point = rtide * point_in_cap( generator, openingAngle);
                dR = launch_point.x;
                dT = launch_point.y;
                doop = launch_point.z;

                ic[0] = satpos.x + rhat.x*dR + that.x*dT + oophat.x*doop + mwpos.x;
                ic[1] = satpos.y + rhat.y*dR + that.y*dT + oophat.y*doop + mwpos.y;
                ic[2] = satpos.z + rhat.z*dR + that.z*dT + oophat.z*doop + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back(ic);
                debris.t.push_back(t);
                debris.phases.push_back(0.0);

                //Leading
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                launch_point = -rtide * point_in_cap( generator, openingAngle);
                dR = launch_point.x;
                dT = launch_point.y;
                doop = launch_point.z;

                ic[0] = satpos.x + rhat.x*dR + that.x*dT + oophat.x*doop + mwpos.x;
                ic[1] = satpos.y + rhat.y*dR + that.y*dT + oophat.y*doop + mwpos.y;
                ic[2] = satpos.z + rhat.z*dR + that.z*dT + oophat.z*doop + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back( ic );
                debris.t.push_back(t);
                debris.phases.push_back(0.0);



            }
        }

        return debris;
    }

private:
    std::function<double (double)> make_strip_probability_func(const Satellites &sats)
    {
        return [&sats](double t){
            double rt = sats.rtide(1, t);
            return 1 - pow(rt, 3) * pow( pow(rt, 2) + pow(sats.get_asat(1), 2), -1.5);
        };
    }

};

