#pragma once

#include "base.h"

class RotatingUniform : public DebrisICGen {

    const double dtcreate;
    const size_t numPerCreation;
    std::mt19937 generator;
    std::normal_distribution<double> distribution;

    const Vec3D AMdirection;
    const double rotvel;

public:
    RotatingUniform(double dtcreate, size_t numPerCreation, double sigma,
                    const Vec3D &AMdirection, double rotvel, int seed = 12345):
                        dtcreate(dtcreate),
                        numPerCreation(numPerCreation),
                        generator(),
                        distribution(0.0, sigma),
                        AMdirection(AMdirection),
                        rotvel(rotvel)
    {
        generator.seed(seed);
    }

    Particles make_debrisIC(const Satellites &sats) {
        Particles parts;

        for( double t = sats.tstart ; t < sats.tend ; t += dtcreate ) {
            double rtide = sats.rtide(0, t);

            Vec3D satpos( sats.x(0,t), sats.y(0,t), sats.z(0,t) );
            Vec3D satvel( sats.vx(0,t), sats.vy(0,t), sats.vz(0,t) );

            double rsat  = satpos.length();

            Vec3D rhat = satpos / rsat;
            Vec3D e1 = rhat - (satAMVec*rhat) * satAMVec;
            e1.normalise();

            Vec3D e2 = satAMVec^e1;
            Vec3D e3 = satAMVec;
            std::array<double, 6> ic;

            double ve1, ve2, ve3;
            for( size_t i = 0 ; i < numPerCreation ; ++i ) {
                // Trailing
                ve1 = satvel * e1 + distribution(generator);
                ve2 = satvel * e2 + distribution(generator) + rotVel;
                ve3 = satvel * satAMVec + distribution(generator);

                ic[0] = satpos.x + rhat.x * rtide;
                ic[1] = satpos.y + rhat.y * rtide;
                ic[2] = satpos.z + rhat.z * rtide;

                ic[3] = ve1*e1.x + ve2*e2.x + ve3*e3.x;
                ic[4] = ve1*e1.y + ve2*e2.y + ve3*e3.y;
                ic[5] = ve1*e1.z + ve2*e2.z + ve3*e3.z;

                parts.state.push_back( ic );
                parts.t.push_back(t);
                parts.phases.push_back(0.0);

                // Leading

                ve1 = satvel * e1 + distribution(generator);
                ve2 = satvel * e2 + distribution(generator) - rotVel;
                ve3 = satvel * satAMVec + distribution(generator);

                ic[0] = satpos.x - rhat.x * rtide;
                ic[1] = satpos.y - rhat.y * rtide;
                ic[2] = satpos.z - rhat.z * rtide;

                ic[3] = ve1*e1.x + ve2*e2.x + ve3*e3.x;
                ic[4] = ve1*e1.y + ve2*e2.y + ve3*e3.y;
                ic[5] = ve1*e1.z + ve2*e2.z + ve3*e3.z;

                parts.state.push_back( ic );
                parts.t.push_back(t);
                parts.phases.push_back(0.0);
            }
        }

        return parts;
    }

};

