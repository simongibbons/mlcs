#pragma once

#include "base.h"

#include <AM_vector_evolver.h>

#include <functional>

class RotatingBurstyFixed : public DebrisICGen {

    double dtcreate;
    size_t numPerCreation;
    std::mt19937 generator;
    std::normal_distribution<double> distribution;

    double rotVel;

    double burstWidth;

    Vec3D L0;
    double Rd;

    std::function<double(double)> strip_prob_fn = [](double t) {return 1.0;};
    bool created_strip_fn = false;

    int seed;

public:
    RotatingBurstyFixed(std::istringstream& iss)
    {
        double sigma, Lx, Ly, Lz;
        if( !(iss >> dtcreate >> numPerCreation >> sigma >> burstWidth
                  >> rotVel >> seed >> Lx >> Ly >> Lz >> Rd) ) {
            throw std::runtime_error("PARSE ERROR: BurstyStripping expects the parameters:\n"
                                      "dtcreate numPerCreation sigma burstWidth\n"
                                      "rotVel seed Lx Ly Lz");
        }

        L0 = Vec3D(Lx, Ly, Lz);
        L0.normalise();

        distribution = std::normal_distribution<double>(0.0, sigma);
        generator.seed(seed);

    }

    void reset_rng() {
        generator.seed(seed);
    }

    Particles make_debrisIC(const Satellites &sats) {
        Particles parts;

        // AMVector amv = make_AMVector(L0, sats.pot, Rd, 0.0, sats, 0.0001);

        // std::function<Vec3D(double)> satAMVecFun = make_AM_functor(amv);

        if( !created_strip_fn ) {
            strip_prob_fn = make_strip_probability_func(sats);
            created_strip_fn = true;
        }

        double strip_prob_max = 0.0;

        std::ofstream fout;
        fout.open("strip_prob.dat", std::ofstream::out);
        for( double t = sats.tstart ; t < sats.tend ; t += 0.0001 ) {
            double sp = strip_prob_fn(t);
            fout << t << " " << sp << std::endl;
            strip_prob_max = sp > strip_prob_max ? sp : strip_prob_max;
        }
        fout.close();

        std::uniform_real_distribution<double> strip_prob_dist(0.0, strip_prob_max);

        for( double t = sats.tstart ; t < sats.tend ; t += dtcreate ) {
            //Skip particle creation if we probabilistically dont want to
            if( strip_prob_dist(generator) > strip_prob_fn(t) ) {
                continue;
            }

            double rtide = sats.rtide(1, t);

            Vec3D mwpos = sats.get_pos(0, t);
            Vec3D mwvel = sats.get_vel(0, t);

            Vec3D satabspos = sats.get_pos(1, t);
            Vec3D satabsvel = sats.get_vel(1, t);

            Vec3D satpos = satabspos - mwpos;
            Vec3D satvel = satabsvel - mwvel;

            double rsat  = satpos.length();

            Vec3D satAMVec = L0;
            satAMVec.normalise();

            Vec3D rhat = satpos / rsat;
            Vec3D e1 = rhat - (satAMVec*rhat) * satAMVec;
            e1.normalise();

            Vec3D e2 = satAMVec^e1;
            Vec3D e3 = satAMVec;
            std::array<double, 6> ic;

            double ve1, ve2, ve3;
            for( size_t i = 0 ; i < numPerCreation ; ++i ) {
                // Trailing
                ve1 = satvel * e1 + distribution(generator);
                ve2 = satvel * e2 + distribution(generator) + rotVel;
                ve3 = satvel * satAMVec + distribution(generator);

                ic[0] = satpos.x + rhat.x * rtide + mwpos.x;
                ic[1] = satpos.y + rhat.y * rtide + mwpos.y;
                ic[2] = satpos.z + rhat.z * rtide + mwpos.z;

                ic[3] = ve1*e1.x + ve2*e2.x + ve3*e3.x + mwvel.x;
                ic[4] = ve1*e1.y + ve2*e2.y + ve3*e3.y + mwvel.y;
                ic[5] = ve1*e1.z + ve2*e2.z + ve3*e3.z + mwvel.z;

                parts.state.push_back( ic );
                parts.t.push_back(t);
                parts.phases.push_back(0.0);

                // Leading

                ve1 = satvel * e1 + distribution(generator);
                ve2 = satvel * e2 + distribution(generator) - rotVel;
                ve3 = satvel * satAMVec + distribution(generator);

                ic[0] = satpos.x - rhat.x * rtide + mwpos.x;
                ic[1] = satpos.y - rhat.y * rtide + mwpos.y;
                ic[2] = satpos.z - rhat.z * rtide + mwpos.z;

                ic[3] = ve1*e1.x + ve2*e2.x + ve3*e3.x + mwvel.x;
                ic[4] = ve1*e1.y + ve2*e2.y + ve3*e3.y + mwvel.y;
                ic[5] = ve1*e1.z + ve2*e2.z + ve3*e3.z + mwvel.z;

                parts.state.push_back( ic );
                parts.t.push_back(t);
                parts.phases.push_back(0.0);
            }
        }

        return parts;
    }

    void output_strip_prob_fn( std::string fname, const Satellites &sats ) {
        strip_prob_fn = make_strip_probability_func(sats);
        created_strip_fn = true;

        std::ofstream fout;
        fout.open( fname, std::ofstream::out );
        if( fout.fail() ) {
            std::cerr << "ERROR: Cannot open file " << fname << " for writing\n";
            return;
        }

        for( double t = sats.tstart; t < sats.tend ; t += 0.001 ) {
            fout << t << " " << strip_prob_fn(t) << std::endl;
        }
        fout.close();
    } 

private:
    std::function<double (double)> make_strip_probability_func(const Satellites &sats)
    {
        std::vector<double> peri_times(find_peri_times(sats));
        std::vector<double> widths;

        //Add an initial period of stripping as well before we have orbit
        //integration data. Assume that the period is the same as the last one
        //(should possibly change this in the future........)
        const double radial_period = peri_times[1] - peri_times[0];

        peri_times.insert(peri_times.begin(), peri_times.front() - radial_period);
        peri_times.insert(peri_times.begin(), peri_times.front() - radial_period);

        peri_times.push_back(peri_times.back() + radial_period);
        peri_times.push_back(peri_times.back() + radial_period);

        for(size_t i = 0 ; i < peri_times.size() - 1 ; ++i ) {
            widths.push_back(burstWidth * (peri_times[i+1] - peri_times[i]));
        }
        widths.push_back(widths.back());

        return [peri_times, widths](double t) {
            double retval = 0.0;
            for(size_t i = 0 ; i < peri_times.size() ; ++i ) {
                retval += exp( -0.5*pow((t - peri_times[i]) / widths[i], 2));
            }
            return retval;
        };
    }

    std::vector<double> find_peri_times(const Satellites &sats)
    {
        std::vector<double> peri_times;
        const double dt = 0.0001;

        for( double t = sats.tstart ; t < sats.tend ;  t += dt ) {
            double r = sats.r(1,t);
            if( sats.r(1, t - dt) > r && sats.r(1, t + dt) > r ) {
                peri_times.push_back(t);
            }
        }

        return peri_times;
    }

};


