#pragma once

#include "base.h"
#include <random>
#include <exception>

#include <util/vec3d.h>

class UniformStripping : public DebrisICGen {

    double dtcreate;
    size_t numPerCreation;
    std::mt19937 generator;
    std::normal_distribution<double> distribution;

public:
    UniformStripping(double dtcreate, size_t numPerCreation, double sigma, int seed):
        dtcreate(dtcreate),
        numPerCreation(numPerCreation),
        generator(),
        distribution(0.0, sigma)
    {
        generator.seed(seed);
    }

    UniformStripping(std::istringstream& iss)
    {
        double sigma;
        int seed;
        if( !(iss >> dtcreate >> numPerCreation >> sigma >> seed) )
        {
            throw std::runtime_error("PARSE ERROR: Uniform Stripping expects the parameters: "
                                     "dtcreate numPerCreation sigma seed");
        }

        distribution = std::normal_distribution<double>(0.0, sigma);
        generator.seed(seed);
    }

    Particles make_debrisIC(const Satellites &sats) {
        Particles debris;

        for( double t = sats.tstart ; t < sats.tend ; t += dtcreate ) {
            double rtide = sats.rtide(1, t);

            Vec3D mwpos = sats.get_pos(0, t);
            Vec3D mwvel = sats.get_vel(0, t);

            Vec3D satpos = sats.get_pos(1, t) - mwpos;
            Vec3D satvel = sats.get_vel(1, t) - mwvel;

            double rsat = satpos.length();
            double vrsat = satpos*satvel / rsat;
            double vtsat = sqrt( satvel.lengthsq() - pow(vrsat, 2) );

            Vec3D rhat(satpos);
            rhat.normalise();
            Vec3D that = satvel - rhat*vrsat;
            that.normalise();

            Vec3D oophat = (rhat^that);

            double vr, vt, voop;
            std::array<double, 6> ic;

            for( size_t i=0 ; i < numPerCreation ; ++i ) {

                //Trailing
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                ic[0] = satpos.x + rhat.x*rtide + mwpos.x;
                ic[1] = satpos.y + rhat.y*rtide + mwpos.y;
                ic[2] = satpos.z + rhat.z*rtide + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back(ic);
                debris.t.push_back(t);
                debris.phases.push_back(0.0);

                //Leading
                vr = vrsat + distribution(generator);
                vt = vtsat + distribution(generator);
                voop = distribution(generator);

                ic[0] = satpos.x - rhat.x*rtide + mwpos.x;
                ic[1] = satpos.y - rhat.y*rtide + mwpos.y;
                ic[2] = satpos.z - rhat.z*rtide + mwpos.z;

                ic[3] = vr*rhat.x + vt*that.x + voop*oophat.x + mwvel.x;
                ic[4] = vr*rhat.y + vt*that.y + voop*oophat.y + mwvel.y;
                ic[5] = vr*rhat.z + vt*that.z + voop*oophat.z + mwvel.z;

                debris.state.push_back( ic );
                debris.t.push_back(t);
                debris.phases.push_back(0.0);


            }
        }

        return debris;
    }
};

