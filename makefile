
CXX=g++
CFLAGS = -Wall -Wextra -Wno-sign-compare -pedantic -c -O3 --std=c++11 -Iinclude `gsl-config --cflags`
LDFLAGS = `gsl-config --libs` -larmadillo -llapack -lblas

# Add these flags to enable generating the debris in parallel.
# if used the number of threads to use can be set with the environment
# variable OMP_NUM_THREADS
CFLAGS += -fopenmp
LDFLAGS += -fopenmp

SRCDIR= src

SOURCES= $(wildcard $(SRCDIR)/**/*.cc) $(wildcard $(SRCDIR)/*.cc)
OBJECTS= $(SOURCES:.cc=.o)
DEPS = $(SOURCES:.cc=.d)

TESTEXE = test_runner
TESTDIR = test

TESTSRC= $(wildcard $(TESTDIR)/**/*.cc) $(wildcard $(TESTDIR)/*.cc)
TESTDEPS= $(TESTSRC:.cc=.d)
TESTOBJ= $(filter-out %main.o, $(OBJECTS))
TESTOBJ += $(TESTSRC:.cc=.o)

TESTLDFLAGS = $(LDFLAGS) -lboost_unit_test_framework

mLCS: $(OBJECTS)
	$(CXX) $(OBJECTS) $(LDFLAGS) -o mLCS

python:
	cd python_wrapper;\
	rm -rf build mLCS.so;\
	python setup.py build_ext --inplace;\
	cp mLCS.so ..

%.o: %.cc
	$(CXX) $(CFLAGS) -MD $< -o $@

$(TESTEXE): $(TESTOBJ)
	$(CXX) $(TESTOBJ) $(TESTLDFLAGS) -o $(TESTEXE)

test: $(TESTEXE)
	./$(TESTEXE) --show_progress

clean:
	rm -f $(OBJECTS)
	rm -f $(DEPS)
	rm -f $(TESTOBJ)
	rm -f $(TESTDEPS)
	rm -f mLCS
	rm -rf $(TESTEXE)
	rm -f mLCS.so

-include $(DEPS)
-include $(TESTDEPS)
