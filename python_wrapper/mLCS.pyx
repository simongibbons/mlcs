from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp.memory cimport shared_ptr
from cython.operator cimport dereference as deref

cimport numpy as np
import numpy as np

# Cython doesn't support integer template arguments so this is what has to be done....
cdef extern from *:
    ctypedef int _SIX_T "6"

cdef extern from "<array>":
    cdef cppclass array[double, _SIX_T]:
        pass


cdef extern from "<satellite.h>":
    cdef cppclass Satellite:
        pass

cdef extern from "<ic_gens/base.h>":
    cdef cppclass DebrisICGen:
        pass


cdef extern from "<stream_params.h>":
    cdef cppclass StreamParams:
        vector[Satellite] sats
        vector[array[double, _SIX_T]] ics
        shared_ptr[DebrisICGen] strip_method

        double tbegin
        double tend
        double dt_sat
        double dt_phase
        
    StreamParams read_params(string) except +

cdef extern from "<satellites.h>":
    cdef cppclass Satellites:
        Satellites() except +
        Satellites(vector[Satellite], vector[array[double, _SIX_T]], double, double, double) except +
        const size_t nsats
        double tstart
        double tend
        double x(int, double)
        double y(int, double)
        double z(int, double)
        double vx(int, double)
        double vy(int, double)
        double vz(int, double)
        double rtide(int, double)


cdef extern from "<debris.h>":
    cdef cppclass Debris:
        Debris(const Satellites&, shared_ptr[DebrisICGen], double) except +
        vector[double] xs
        vector[double] ys
        vector[double] zs
        vector[double] vxs
        vector[double] vys
        vector[double] vzs
        vector[double] phases
        vector[double] tcreate
        vector[double] tstrips


def make_stream(string param_str):
    cdef StreamParams params = read_params(param_str)
    cdef Satellites* sats = NULL
    cdef Debris* debris = NULL

    try:
        sats = new Satellites(params.sats, params.ics,\
                              params.tbegin, params.tend,\
                              params.dt_sat)
    except Exception as e:
        if sats != NULL:
            del sats
        raise e

    cdef np.ndarray[double, ndim=1] t = np.linspace(sats.tstart, sats.tend, int((sats.tend - sats.tstart)/0.001))
    cdef np.ndarray[double, ndim=3] orbits = np.empty((sats.nsats, t.shape[0], 8))

    for i in xrange(sats.nsats):
        for j in xrange(t.shape[0]):
            orbits[i, j, 0] = t[j]
            orbits[i, j, 1] = sats.x(i, t[j])
            orbits[i, j, 2] = sats.y(i, t[j])
            orbits[i, j, 3] = sats.z(i, t[j])
            orbits[i, j, 4] = sats.vx(i, t[j])
            orbits[i, j, 5] = sats.vy(i, t[j])
            orbits[i, j, 6] = sats.vz(i, t[j])
            orbits[i, j, 7] = sats.rtide(i, t[j])


    try:
        debris = new Debris(deref(sats), params.strip_method, params.dt_phase)
    except Exception as e:
        if sats != NULL:
            del sats
        if debris != NULL:
            del debris
        raise e

    cdef np.ndarray[double, ndim=2] parts = np.empty((debris.xs.size(), 9))

    for i in xrange(debris.xs.size()):
        parts[i, 0] = debris.xs[i] - sats.x(0, sats.tend)
        parts[i, 1] = debris.ys[i] - sats.y(0, sats.tend)
        parts[i, 2] = debris.zs[i] - sats.z(0, sats.tend)
        parts[i, 3] = debris.vxs[i] - sats.vx(0, sats.tend)
        parts[i, 4] = debris.vys[i] - sats.vy(0, sats.tend)
        parts[i, 5] = debris.vzs[i] - sats.vz(0, sats.tend)
        parts[i, 6] = debris.phases[i]
        parts[i, 7] = debris.tcreate[i]
        parts[i, 8] = debris.tstrips[i]

    # Cleanup before we return
    if sats != NULL:
        del sats
    if debris != NULL:
        del debris

    return (parts, orbits)