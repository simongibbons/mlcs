from distutils.core import setup, Extension
from Cython.Build import cythonize

extensions = [Extension(
                "mLCS",
                sources=["mLCS.pyx", "../src/stream_params.cc", "../src/util/vec3d.cc", "../src/util/rng.cc"],
                include_dirs=["../include"],
                libraries=["gsl", "gslcblas", "m", "armadillo"],
                extra_compile_args=["-O3", "--std=c++11", "-fopenmp"],
                extra_link_args=["--std=c++11", "-fopenmp"],
                language="c++"
    )]

setup(
    ext_modules = cythonize(extensions)
)