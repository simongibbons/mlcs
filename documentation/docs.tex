\documentclass{article}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{amsmath}

\usepackage{times}


\usepackage{color}
\definecolor{darkred}{rgb}{0.80,0.00,0.05}
\definecolor{blue}{rgb}{0.00,0.00,0.95}
\definecolor{darkgreen}{rgb}{0.00,0.60,0.0}
\definecolor{gray}{rgb}{0.9,0.9,0.9}


\usepackage{listings}
\lstset{backgroundcolor=\color{gray}}
\lstset{breaklines=true}
\lstset{basicstyle=\ttfamily\small}
\lstset{showstringspaces=false}
\lstset{keywordstyle=\color{blue}\bfseries}
\lstset{commentstyle=\ttfamily\small\color{darkgreen}}
%\lstset{numbers=left,numberstyle=\tiny}
\lstset{columns=fixed,basewidth=0.5em}

\usepackage{natbib}
\input{journal_defs}

\title{mLCS User Guide}
\author{Simon Gibbons}

\begin{document}

\maketitle

This is the user guide for the mLCS code for stream generation. A detailed description
of the algorithm can be found in \citet{Gibbons2014}.

\section{Installation}

mLCS depends upon a number of packages:

\begin{itemize}
 \item Boost
 \item GSL
 \item Armadillo
\end{itemize}

In addition to this you will need a more modern version of GCC than is provided by
default as the code is written in the modern {\tt c++11} dialect.

The simplest way of getting these setup on the IoA science cluster is to use the
versions provided in the module environment. To do this you should add the following
lines to your {\tt .profile} file (which should be located in your home directory)
and open a new terminal.

\begin{lstlisting}[language=Bash]
source  /opt/ioa/Modules/default/init/bash

module load gcc/4.7.2
module load python/2.7
module load openmpi/gcc/4.7.2/1.6.5
module load boost/1.57.0
module load armadillo/3.930.2
module load gsl/1.16
\end{lstlisting}

Once this is done you can grab the code from the bitbucket repository. The easiest
way of doing this will be to clone the repo with git with the command:

\begin{lstlisting}
git clone https://bitbucket.org/simongibbons/mlcs.git
\end{lstlisting}

this will create a new directory called {\tt mlcs} with the code in.

To compile the code you should just need to move into this new directory and
run

\begin{lstlisting}
make
\end{lstlisting}

If all has gone well then you should see a new executable called {\tt mLCS}.

To use this to generate a stream you need to run this with a parameter file with

\begin{lstlisting}
./mLCS parameter_file.txt
\end{lstlisting}

\subsection{Updating}

To update your installed version you need to pull any changes made from the
bitbucket repository to your local version. To do this move into your {\tt mlcs}
directory and run

\begin{lstlisting}
git pull
\end{lstlisting}

Then recompile your code.

\section{The Parameter File}

This file determines the parameters of the system you wish to run.

It begins with a number of definitions for the particles which will source
potentials. The first of these always represents the Milky Way and it is from
this quantities such as the tidal radius of the other particles are calculated.

The second of these will be the particle that is the progenitor of the stream.

Any other additional particles will just act as perturbers of the others and
the test particles created in the stream.

Each of these follow the same general format:

\begin{lstlisting}[language=Bash]
BeginSat
Potential NFW 0.00356 9.28 #The potential sourced by that particle
mass 100.0                 #Mass used for dynamical friction and rtide
asat 1.0                   #scale radius used with some stripping methods
ic 0 0 0 0 0 0             #initial position and velocity
EndSat
\end{lstlisting}

The parameters for each of the potentials are listed in section~\ref{sec:potential}

Note that you can create particles which source multiple potentials - e.g. if you
wish to create a multi-component Milky Way model consisting of a Bulge, Disk and Halo.
To do this just add more Potential lines to the Satellite.

The next lines consist of options controlling the integration options - you really
should only need to change {\tt tbegin} and {\tt tend} here.

\begin{lstlisting}[language=Bash]
Integration tbegin 0.0     #Time at which the simulation starts
Integration tend 4.22      #Time at wihch the simulation ends
Integration dt_sat 0.0001  #Timestep at which to store satellite positions
Integration dt_phase 0.01  #Timestep to calculate phase proxy for debris
\end{lstlisting}

Note that the code is setup to perform backwards integration from a known
end positions of your satellites. To use this set $\mathtt{tbegin} > \mathtt{tend}$
then the {\tt ic}'s that you set in the parameter files are used as the end point
and the code integrates backwards from those locations.

The final line of the parameter file contains the method of debris production you
wish to use

\begin{lstlisting}
Stripping BurstyRtide 0.00001 1 8.0 1232131
\end{lstlisting}

A discription of each of the methods that are defined, and their parameters is in
section~\ref{sec:strip}.

You can comment your parameter file - any lines beginning with a \#
symbol will be ignored by the code.

An example parameter file is distributed with the code in {\tt params\_example.txt}

\section{Output Files}

The code produces it's output in plain text files the structure of which is listed
below. All of the units of these are in internal units as defined in section
\ref{sec:units}.

Note that in the current version of the code all of the orbit integration is done
in the \emph{inertial} frame, i.e. one in which the Milky Way is free to move under
the effect of the other particles. This is done with the method described in
\citet{Gomez2015}.

\subsection{debris.dat}

This file contains the created stream in a coordinate system centered on - and moving
with the final velocity of the Milky Way particle.

\begin{center}
\begin{tabular}[h]{ccc}
\hline
Column Number & Name & Description\\
\hline
1 & $x$ & \\
2 & $y$ & Particle coordinates\\
3 & $z$ & \\
\hline
4 & $v_x$ & \\
5 & $v_y$ & Particle velocities\\
6 & $v_z$ & \\
\hline
7 & $\chi$ & Phase proxy as defined in \citet{Gibbons2014} \\
8 & $t_{\rm create}$ & Time the particle was created\\
9 & $t_{\rm strip}$ & Time the particle was stripped\\
\hline
\end{tabular}
\end{center}

\subsection{satN.dat}

The code will create $N$ of these files (generally $N \geq 2$)
for each of for the number of potential
sourcing particles in the simulation. The first of these will be for the particle
representing the Milky Way, the second of these will be for the progenitor of the
stream. Any after this will be for any additional particles you create (e.g.
one representing the LMC).

They all have the following format - note that here all the positions and velocities
are quoted in the inertial frame so that the Milky Way's motion isn't subtracted off.

\begin{center}
\begin{tabular}[h]{ccc}
\hline
Column Number & Name & Description\\
\hline
1 & $t$ & Time\\
\hline
2 & $x$ & \\
3 & $y$ & Position\\
4 & $z$ & \\
\hline
5 & $v_x$ & \\
6 & $v_y$ & Velocity\\
7 & $v_z$ & \\
\hline
8 & $r_{\rm tide}$ & Tidal Radius\\
\hline
\end{tabular}
\end{center}

\subsection{strip\_prob.txt}

This file contains the probability function used when stripping particles - used
whenever one of the bursty stripping modes is used.

It contains two columns the time and the probability of producing a particle at
that time.

\section{A Worked Example}

The following parameter file generates the example stream used in Figure~5 of
\citet{Gibbons2014} and should act as a baseline test to see if you have got
the code working.

Here a Sgr like progenitor is put on an orbit in the $(x,z)$ plane of a Milky Way
represented with an NFW halo.

\begin{lstlisting}
BeginSat
Potential NFW 0.0035659772866 9.28311915976
mass 100.0
asat 1.0
ic 0 0 0 0 0 0
EndSat

BeginSat
Potential Plummer 0.086 1.0
mass 0.086
asat 1.0
ic 0.0 0.0 70.0 80.0 0.0 0.0
EndSat

Integration tbegin 0.0
Integration tend 4.22
Integration dt_sat 0.0001
Integration dt_phase 0.01

Stripping UniformStripping 0.0005 1 8.0 1232131
\end{lstlisting}

If you plot the $(x,z)$ plane of the stream then you should see the same output
as in Figure~\ref{fig:stream}.


\begin{figure}[th]
\centering
\includegraphics[width=0.7\textwidth]{stream.pdf}
\caption{The expected output of the example parameter file}\label{fig:stream}
\end{figure}


\section{Units}\label{sec:units}

The system of units used in mLCS are the same as the defaults in {\sc gadget}. That
is masses are expressed in $10^{10} M_\odot$, distances are in kpc and velocities
are in km/s.

This leads to the natural unit of time being the ${\rm kpc} / ({\rm km~s^{-1}}) = 0.978 {\rm Gyr}$
or as I like to refer to that mouthful - the `almost' gigayear.

With these choices the gravitational constant is $G = 43007.1$ in internal units.



\section{Potentials}\label{sec:potential}

Note that not all the options in the code will work with all potentials - this is
because not all the properties of the potentials are defined by default. For example
the dynamical friction model needs the velocity dispersion profile $\langle v^2 \rangle$
to be defined for it.

\subsection{NFW}

The NFW potential is a commonly used model for Dark Matter halos as it matches
the results from dark matter only cosmological simulations well. It is defined
by the density

\begin{equation}
\rho = \frac{\rho_0}{(r/r_s)(1 + r/r_s)^2}
\end{equation}

\begin{lstlisting}
Potential NFW {rho0} {rs}
\end{lstlisting}

\subsection{Plummer}

The plummer profile a commonly used mass profile in dynamics - mostly because
it has a distribution function which is very easy to sample from. It's density is
defined as:

\begin{equation}
\rho = \frac{3M}{4\pi a^3} \left(1 + \frac{r^2}{a^2} \right)^{-5/2}
\end{equation}

\begin{lstlisting}
Potential Plummer {M} {a}
\end{lstlisting}

\section{Stripping Methods}\label{sec:strip}

The code has a number of methods defined for stripping. Each of the options have
a few parameters which are common between them

\begin{description}
\item [tstrip] \hfill\\
   How often debris particles are created, or in the case of the bursty stripping modes
how often we attempt to create a particle (one is created with a probability defined by the method in that case)
\item [numpercreation] \hfill\\
   How many particles are created at each creation time - this should generally always be set to 1.
\item [sigma] \hfill\\
   The velocity dispersion of stripped particles.
\item [seed] \hfill\\
   The seed of the random number generator used for creation of the particle properties
\end{description}

Any additional options will be given with the discription of the method.

\subsection{UniformStripping}

In this method particles are created uniformly along the orbit of the progenitor with
particles being created every {\tt tstrip}

\begin{lstlisting}
Stripping UniformStripping {tstrip} {numpercreation} {sigma} {seed}
\end{lstlisting}

\subsection{BurstyStripping}

Here stripping is implemented with a probability given by a gaussian centered on
the pericentres of the progenitor's orbit with a width given by

\begin{equation}
\mathtt{burst\_width} \times \mathtt{Orbital~Period}
\end{equation}

\begin{lstlisting}
Stripping BurstyStripping {tstrip} {numpercreation} {burst_width} {sigma} {seed}
\end{lstlisting}

\section{Dynamical Friction}\label{sec:df}

Dynamical friction is implimented using the Chandrashkar formula

\bibliographystyle{mn2e}
\bibliography{refs}


\end{document}
