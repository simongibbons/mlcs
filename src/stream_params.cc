#include <stream_params.h>
#include <potential/all.h>

#include <ic_gens/all.h>
#include <dynamical_friction/all.h>

#include <iostream>
#include <string>

#include <exception>

class ParseError : public std::exception
{
  std::string error_string;

public:
  ParseError(const std::string& s):error_string("PARSE ERROR " + s)
  {}

  virtual const char* what() const throw()
  {
    return error_string.c_str();
  }
};

std::unique_ptr<Potential> construct_potential(std::istringstream& iss)
{
    std::string potential_type;

    iss >> potential_type;
    auto it = potential_map.find(potential_type);

    if( it == potential_map.end() ) {
        throw ParseError("Unknown Potential " + potential_type);
    }
    return it->second(iss);
}

std::shared_ptr<DynamicalFriction> construct_df_method( std::istringstream& iss )
{
    std::string df_type;

    iss >> df_type;
    auto it = df_map.find(df_type);
    if( it == df_map.end() ) {
        throw ParseError("Unknown DF Type " + df_type);
    }
    return it->second(iss);
}


std::shared_ptr<DebrisICGen> construct_stripping_method( std::istringstream& iss)
{
    std::string strip_type;
    iss >> strip_type;

    auto it = ic_map.find(strip_type);
    if( it == ic_map.end() ) {
        throw ParseError("Unknown Stripping Type " + strip_type);
    }

    return it->second(iss);
}

void parse_integration_var( std::istringstream& iss, StreamParams& params)
{
    std::string var_type;
    iss >> var_type;

    if( var_type == "tbegin" ) {
        iss >> params.tbegin;
    }
    else if( var_type == "tend" ) {
        iss >> params.tend;
    }
    else if( var_type == "dt_sat" ) {
        iss >> params.dt_sat;
    }
    else if( var_type == "dt_phase" ) {
        iss >> params.dt_phase;
    }
    else {
        throw ParseError("Unknown Integration Parameter " + var_type);
    }
}

StreamParams read_params(const std::string &s_in )
{
    StreamParams params;

    std::stringstream fin;
    fin << s_in;

    std::string line;
    while( std::getline(fin, line) ) {
        if( line.size() == 0 ) continue;

        std::istringstream iss(line);
        std::string param_type;
        iss >> param_type;

        if( param_type.size() == 0 || param_type[0] == '#' ) continue;

        if(param_type == "BeginSat") {
            std::shared_ptr<CompositePotential> pot = std::make_shared<CompositePotential>();
            Satellite s(0.0, 0.0, pot);
            while( std::getline(fin, line) ) {
                if( line.size() == 0 ) continue;
                std::istringstream iss2(line);

                std::string sat_param_type;
                iss2 >> sat_param_type;

                if( sat_param_type.size() == 0 || sat_param_type[0] == '#' ) continue;

                if( sat_param_type == "EndSat") {
                    if( pot->num_potentials() == 0 ) {
                        throw ParseError("At least one potential must be given");
                    }
                    if( pot->num_potentials() == 1 ) {
                        s.pot = pot->convert_to_single_potential();
                    }
                    else {
                        s.pot = pot;
                    }
                    params.sats.push_back(s);
                    break;
                }
                else if(sat_param_type == "Potential") {
                    pot->potentials.push_back( construct_potential(iss2) );
                }
                else if(sat_param_type == "mass") {
                    iss2 >> s.mass;
                }
                else if(sat_param_type == "asat") {
                    iss2 >> s.asat;
                }
                else if(sat_param_type == "ic") {
                    double x, y, z, vx, vy, vz;
                    iss2 >> x >> y >> z >> vx >> vy >> vz;
                    params.ics.push_back( {{x, y, z, vx, vy, vz}} );
                }
                else if(sat_param_type == "DynamicalFriction") {
                    s.df = construct_df_method(iss2);
                }
                else {
                    throw ParseError("Unknown Variable Type: " + sat_param_type);
                }
            }
        }
        else if(param_type == "Integration") {
            parse_integration_var(iss, params);
        }
        else if(param_type == "Stripping") {
            params.strip_method = construct_stripping_method(iss);
        }
        else {
            throw ParseError("Unknown Variable Type: " + param_type);
        }
    }

    return params;
}

