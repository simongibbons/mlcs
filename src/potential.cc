#include <potential/base.h>

#include <gsl/gsl_monte_vegas.h>

double m_of_r_integrand(double *k, size_t dim, void *params)
{
    Potential *this_ptr = static_cast<Potential *>(params);
    double x = k[0] * sin(k[1]) * cos(k[2]);
    double y = k[0] * sin(k[1]) * sin(k[2]);
    double z = k[0] * cos(k[1]);


    return pow(k[0], 2) * sin(k[1]) * this_ptr->rho(x, y, z, 0.0);
}

// Default method which will do the triple integral over the density to
// Find the mass enclosed - this is useful for when I'm lazy but shouldn't
// necissarilly be relied upon.
double Potential::m_of_r(double r)
{
    double xl[3] = {0.0, 0.0, 0.0};
    double xu[3] = {r, pi, 2*pi};

    double res, err;

    gsl_monte_function G = {&m_of_r_integrand, 3, this};

    gsl_monte_vegas_state *s = gsl_monte_vegas_alloc(3);

    gsl_rng *rng = gsl_rng_alloc(gsl_rng_default);


    gsl_monte_vegas_integrate(&G, xl, xu, 3, 10000, rng, s, &res, &err);

    do {
        gsl_monte_vegas_integrate(&G, xl, xu, 3, 100000, rng, s, &res, &err);
    } while(fabs(gsl_monte_vegas_chisq(s) - 1.0) > 0.5);

    gsl_monte_vegas_free(s);
    gsl_rng_free(rng);

    return res;
}