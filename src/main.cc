#include <iostream>
#include <fstream>
#include <exception>
#include <string>

#include <satellites.h>
#include <debris.h>
#include <stream_params.h>

void run(StreamParams& params)
{
    Satellites sats(params.sats, params.ics, params.tbegin, params.tend, params.dt_sat);
    std::ofstream fout;
    for( size_t i=0 ; i < sats.nsats ; ++i ) {
        std::string fname = "sat" + std::to_string(i) + ".dat";
        fout.open(fname, std::ofstream::out);
        for( double t = sats.tstart ; t < sats.tend ; t += 0.001 )
        {
            fout << t << " "
                 << sats.x(i,t) << " "
                 << sats.y(i,t) << " "
                 << sats.z(i,t) << " "
                 << sats.vx(i,t) << " "
                 << sats.vy(i,t) << " "
                 << sats.vz(i,t) << " "
                 << sats.rtide(i,t) << " "
                 << std::endl;
        }
        fout.close();
    }

    params.strip_method->output_strip_prob_fn( "strip_prob.txt", sats);

    Debris debris(sats, params.strip_method, params.dt_phase);
    fout.open("debris.dat", std::ofstream::out);

    for( size_t i = 0 ; i < debris.xs.size() ; ++i ) {
        fout << debris.xs[i] - sats.x(0, sats.tend) << " "
             << debris.ys[i] - sats.y(0, sats.tend) << " "
             << debris.zs[i] - sats.z(0, sats.tend) << " "
             << debris.vxs[i] - sats.vx(0, sats.tend) << " "
             << debris.vys[i] - sats.vy(0, sats.tend) << " "
             << debris.vzs[i] - sats.vz(0, sats.tend) << " "
             << debris.phases[i] << " "
             << debris.tcreate[i] << " "
             << debris.tstrips[i] << " "
             << std::endl;
    }

}

int main(int argc, char *argv[])
{
    std::string pfname;
    if( argc < 2 ) {
        std::cerr << "No parameter file given - defaulting to 'params.txt'\n";
        pfname = "params.txt";
    } else {
        pfname = argv[1];
    }

    std::ifstream fin;
    fin.open(pfname);
    if( !fin.is_open() ) {
        std::cerr << "ERROR: cannot open parameter file: " << argv[1] << std::endl;
        return 2;
    }

    StreamParams params;
    try {
        std::stringstream ss;
        ss << fin.rdbuf();
        params = read_params(ss.str());
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 3;
    }
    fin.close();

    run(params);

    return 0;
}

