#include <util/rng.h>

#include <random>
#include <util/vec3d.h>

#include <consts.h>

Vec3D point_in_cap(std::mt19937 &generator, double opening_angle)
{
    if( opening_angle < 1e-16 ) {
        return Vec3D(1.0, 0.0, 0.0);
    }

    double costheta_lim = cos( pi/2 - opening_angle );

    std::uniform_real_distribution<double> cos_theta_dist(-costheta_lim, costheta_lim);
    std::uniform_real_distribution<double> phi_dist(-opening_angle, opening_angle);

    double theta, phi;
    do {
        theta = acos( cos_theta_dist(generator) );
        phi = phi_dist(generator);
    } while( acos( sin(theta) * cos(phi) ) > opening_angle );

    return Vec3D( sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) );
}
